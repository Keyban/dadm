package uam.kevin_bachmann.models;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author Kevin Bachmann
 */
public class Round implements Serializable {

    private String tablero;
    private String players;
    private int roundId;
    private Timestamp date;
    private int turn;

    public Round(String tablero, String players, int roundId, Timestamp date, int turn) {
        this.tablero = tablero;
        this.players = players;
        this.roundId = roundId;
        this.date = date;
        this.turn = turn;
    }


    public String getTablero() {
        return tablero;
    }

    public void setTablero(String tablero) {
        this.tablero = tablero;
    }

    public String getPlayers() {
        return players;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    public int getRoundId() {
        return roundId;
    }

    public void setRoundId(int roundId) {
        this.roundId = roundId;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }
}
