package uam.kevin_bachmann.models;

/**
 * Model of a user.
 * @author Kevin Bachmann
 */
public class User {

    private int id;
    private String playerId;
    private String email;
    private String password;
    private String name;
    private int lastPartida;

    public User(int id, String playerId, String email, String password, String name, int lastPartida){
        this.id = id;
        this.setPlayerId(playerId);
        this.email = email;
        this.password = password;
        this.name = name;
        this.lastPartida = lastPartida;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLastPartida() {
        return lastPartida;
    }

    public void setLastPartida(int lastPartida) {
        this.lastPartida = lastPartida;
    }

    public int getId() {
        return id;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }
}
