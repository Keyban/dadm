package uam.kevin_bachmann.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uam.kevin_bachmann.R;

/**
 * @author Kevin Bachmann
 */
public class TableroMainFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_tablero_main, container);
        return view;
    }
}
