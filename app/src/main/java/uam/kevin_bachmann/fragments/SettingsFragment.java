package uam.kevin_bachmann.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import uam.kevin_bachmann.R;
import uam.kevin_bachmann.activities.SettingsActivity;
import uam.kevin_bachmann.utils.DataProvider;

/**
 * Fragment which listens to the changes of the settingsview and sets all the
 * settings data.
 * @author Kevin Bachmann
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister the listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(SettingsActivity.NAME_KEY.equals(key)){
            String name = sharedPreferences.getString(SettingsActivity.NAME_KEY, "Humano");
            DataProvider.getInstance().setName(name);
            DataProvider.getInstance().getCurrentUser().setName(name);
        }
    }
}
