package uam.kevin_bachmann.game;

/**
 * 
 * @author Kevin Bachmann
 *
 * This class is the representative for all the constants in the program.
 * (This is done for further work. To be flexible when the program gets bigger)
 */
public class Constantes {
	public static final int DIMENSION = 3;
}
