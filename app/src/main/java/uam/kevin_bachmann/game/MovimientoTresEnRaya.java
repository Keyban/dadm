package uam.kevin_bachmann.game;

import es.uam.eps.multij.Movimiento;

/**
 * This class describes a movement of a player. 
 * In this game its to set a mark on the specific place.
 * @author Kevin Bachmann
 *
 */
public class MovimientoTresEnRaya extends Movimiento {

	private int x;
	private int y;
	
	public MovimientoTresEnRaya(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "("+ x+ "," +y+")";
	}

	/**
	 * Returns true if the object is a movement and the x and y 
	 * coordinates are equal.
	 */
	@Override
	public boolean equals(Object o) {
		if(o instanceof MovimientoTresEnRaya){
			MovimientoTresEnRaya m = (MovimientoTresEnRaya)o;
			if(this.x == m.getX() && this.y == m.getY()){
				return true;
			}
			return false;
		}
		else{
			return false;
		}
	}

	public int getY() {
		return y;
	}

	public int getX() {
		return x;
	}
}
