package uam.kevin_bachmann.game;

import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import es.uam.eps.multij.Evento;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Tablero;
import uam.kevin_bachmann.models.Round;
import uam.kevin_bachmann.utils.DataProvider;
import uam.kevin_bachmann.utils.ServerInterface;

/**
 * This class represents a human player which plays against either
 * against another human player or against a computer-player.
 * The human player has to insert the X and Y coordinates of the wanted
 * Movement into the console when.
 * @author Kevin Bachmann
 *
 */
public class JugadorHumano implements Jugador, Serializable {

    protected String nombre;
    protected String playerId;
    protected String email;

    protected Game game;
    protected ServerInterface server;

	public JugadorHumano(String nombre, String email, Game game, String playerId) {
		this.nombre = nombre;
        this.playerId = playerId;
        this.email = email;

        this.game = game;
        this.server = ServerInterface.getServer(game.getContext());
	}

	/**
	 * Reacts on a specific event like CAMBIO, CONFIRMA and TURNO.
	 * 
	 * CAMBIO: update the screen and check if the games is done or draw
	 * CONFIRMA: nada
	 * TURNO: this event is called when the player has the chance to play.
	 * read in the coordinates to set the cross and check if this move is valid.
	 */
	@Override
	public void onCambioEnPartida(Evento evento) {
		switch (evento.getTipo()) {
            case Evento.EVENTO_CAMBIO:
                game.eventoCambio(evento);
                break;
	        case Evento.EVENTO_TURNO:
                game.eventoTurno(evento, playerId);
	            break;
		}
	}

	/**
	 * Return the name of the player.
	 */
	@Override
	public String getNombre() {
		return nombre;
	}

    @Override
    public boolean puedeJugar(Tablero tablero) {
        final DataProvider provider = DataProvider.getInstance();
        if(provider.isOnline()){
            final boolean[] done = {false};
            final boolean[] isMyTurn = {false};
            final int[] turn = {0};
            server.isMyTurn(playerId, provider.getCurrentRound().getRoundId(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {
                    try {
                        turn[0] = jsonObject.getInt("turn");
                        Round r = provider.getCurrentRound();
                        r.setTablero(jsonObject.getString("codedboard"));
                        provider.setCurrentRound(r);

                        provider.setUltimateMovement(jsonObject.getString("codedboard"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(turn[0]==1){
                        isMyTurn[0] = true;
                    }
                    done[0] = true;
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Toast.makeText(game.getContext(), volleyError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
            while(!done[0]){
                // wait for completion
            }
            return isMyTurn[0];
        }else{
            return true;
        }
    }

    public void setPlayerId(String id){
        this.playerId = id;
    }

    public String getPlayerId(){
        return this.playerId;
    }

    public String getEmail(){
        return this.email;
    }
}
