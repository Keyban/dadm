package uam.kevin_bachmann.game;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.JugadorAleatorio;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;
import uam.kevin_bachmann.R;
import uam.kevin_bachmann.activities.SettingsActivity;
import uam.kevin_bachmann.activities.TableroActivity;
import uam.kevin_bachmann.utils.DataProvider;
import uam.kevin_bachmann.utils.IsMyTurnService;
import uam.kevin_bachmann.utils.ServerInterface;

/**
 * This class represents the whole Game TresEnRaya with all the rules and events.
 * From this class all the events and turns are controlled.
 *
 * @author Kevin Bachmann
 */
public class Game {

    private Jugador jugador;
    private Jugador jugadorRemoto;

    private Partida partida;

    private TableroActivity main;
    private Context ctx;

    private ServerInterface server;
    private DataProvider provider;

    private Thread gameThread;

    private int nextX;
    private int nextY;
    private int movements;

    private int turn;

    private Evento eventoTurno;
    public static Timer timer;
    public static final int TIMER_SEC = 10;

    public static final String ISMYTURN_SERVICE =
            "uam.kevin_bachmann.ISMYTURN";

    public static BroadcastReceiver receiver;

    public Game(TableroActivity main){
        this.main = main;
        this.ctx = main.getApplicationContext();
        this.server = ServerInterface.getServer(ctx);
        this.provider = DataProvider.getInstance();

        this.partida = null;
        this.gameThread = null;

        this.nextX = -1;
        this.nextY = -1;
        this.movements = 0;

        if(provider.isOnline()) {
            startTimer();
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    stopTimer();
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
//                        realizeRemoteMovement();
                            executeTurn();
                        }
                    };
                    thread.start();
                }
            };
        }
    }

    public void startTimer(){
        if(timer == null)
            timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(ctx, IsMyTurnService.class);
                provider.setCurrentPlayer((JugadorHumano) jugador);
                ctx.startService(intent);
            }
        }, TIMER_SEC * 1000, TIMER_SEC * 1000);
    }

    public void stopTimer(){
        if(timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }

    /**
     * Start a new game with the defined players. If the parameter thread is true
     * a new thread with the game is created and otherwise the current thread is used.
     * @param thread boolean which indicates if a new thread shall be produced or not
     */
    public void start(boolean thread){
        JugadorAleatorio jugadorAleatorio = new JugadorAleatorio(ctx.getString(R.string.playerMachine));

        ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
        if(provider.isOnline()){

            jugador = new JugadorHumano(getName(), provider.getCurrentUser().getEmail(), this, provider.getCurrentUser().getPlayerId());
            jugadores.add(jugador);

            String name;
            String[] parts = provider.getCurrentRound().getPlayers().split(",");
            if(parts[0].equals(provider.getCurrentUser().getEmail())){
                name = parts[1];
            }else{
                name = parts[0];
            }
            jugadorRemoto = new JugadorHumano(name, name, this, "");
            jugadores.add(jugadorRemoto);

        }else{
            jugador = new JugadorHumano(getName(), provider.getCurrentUser().getEmail(), this, "");
            jugadores.add(jugador);
            jugadores.add(jugadorAleatorio);
        }

        final Partida partida = new Partida(new TableroTresEnRaya(), jugadores);
        if(!thread){
            partida.comienzaPartida();
        }
        else{
            if(gameThread != null && gameThread.isAlive()){
                return;
            }
            gameThread = new Thread() {
                @Override
                public void run() {
                    partida.comienzaPartida();
                }
            };
            gameThread.start();
        }
    }

    /**
     * Get player name from preferences
     * @return set player name
     */
    private String getName(){
        String def = ctx.getString(R.string.player1);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        return sharedPreferences.getString(SettingsActivity.NAME_KEY, def);
    }

    /**
     * Set current clicked x and y coordinates
     * @param x x value of the button
     * @param y y value of the button
     */
    public void nextTurn(int x, int y){
        this.nextX = x;
        this.nextY = y;
    }

    /**
     * Change Event. This event is called whenever a change occurs in the game.
     * Here is checked if somebody has won and the display is updated.
     * @param evento the event which contains all the information about the current game-state
     */
    public void eventoCambio(Evento evento){
        if(evento != null){
            partida = evento.getPartida();
        }

        // get tablero from partido
        TableroTresEnRaya t = (TableroTresEnRaya)partida.getTablero();
        Jugador jugador = partida.getJugador(t.getTurno());

        MovimientoTresEnRaya m = (MovimientoTresEnRaya)t.getUltimoMovimiento();
        if (m != null){
            main.updateTablero(m.getX(), m.getY(), t.getTablero()[m.getX()][m.getY()]);
        }

        if(movements > 0) {
            if (t.getEstado() == Tablero.FINALIZADA) {
                main.showToast(ctx.getString(R.string.finished) + " " + jugador.getNombre(), Toast.LENGTH_SHORT);
                if (jugador.getNombre().equals(DataProvider.getInstance().getCurrentUser().getName())) {
                    provider.updateStatistics(0, 1, movements);
                    if(provider.isOnline())
                        provider.updateStatistics(((JugadorHumano)partida.getJugador((turn)%2)).getEmail(), 1,0, movements-1);
                } else {
                    provider.updateStatistics(1, 0, movements);
                    if(provider.isOnline())
                        provider.updateStatistics(((JugadorHumano)partida.getJugador((turn)%2)).getEmail(), 0,1, movements-1);
                }
                movements = 0;
            } else if (t.getEstado() == Tablero.TABLAS) {
                movements = 0;
                main.showToast(ctx.getString(R.string.draw), Toast.LENGTH_SHORT);
            }

            if(!provider.isOnline())
                main.setPlayerText(jugador.getNombre());
            else
                main.setPlayerText(partida.getJugador(turn-1).getNombre());
        }
    }

    /**
     * Event new Turn. That means a human player can choose a new field to put a mark in.
     * Here the game waits until the player selects his field and executes the turn.
     * @param evento the event which contains all the information about the current game-state
     */
    public void eventoTurno(Evento evento, String playerId){
        // get tablero from partido
        TableroTresEnRaya t = (TableroTresEnRaya)evento.getPartida().getTablero();
        jugador = evento.getPartida().getJugador(t.getTurno());
        JugadorHumano j = (JugadorHumano)jugador;
        j.setPlayerId(playerId);
        provider.setCurrentPlayer(j);
        this.eventoTurno = evento;

        if(!provider.isOnline()){
            executeTurn();
        }
        else {
            if(playerId.isEmpty()){
                //second player.. do nothing
                return;
            }
            Intent intent = new Intent(ctx, IsMyTurnService.class);
            ctx.startService(intent);
        }
    }

    public void executeTurn(){
        if(provider.isOnline()) {
            stopTimer();
            // init new tablero
            if (provider.getUltimateMovement() != null && !provider.getUltimateMovement().isEmpty()) {
                rebuildTablero(provider.getUltimateMovement());
                eventoCambio(null);
            }
        }
        else{
            partida = eventoTurno.getPartida();
        }

//        partida = eventoTurno.getPartida();
        TableroTresEnRaya t = (TableroTresEnRaya) partida.getTablero();
        JugadorHumano j = (JugadorHumano) partida.getJugador(t.getTurno());

        MovimientoTresEnRaya m;
        boolean done=true;
        int x;
        int y;
        // check in a loop if the inserted values are alright
        do{
            if(!done){
                main.showToast(ctx.getString(R.string.invalid), Toast.LENGTH_SHORT);
            }

            if(provider.isOnline()) {
                main.setPlayerText(partida.getJugador(turn - 1).getNombre());
                t.setElement(turn);
            }else{
                main.setPlayerText(jugador.getNombre());
            }

            while(nextX == -1 || nextY == -1){
                //wait for the player to play
            }
            m = new MovimientoTresEnRaya(nextX,nextY);
            x = nextX; y = nextY;
            nextX = -1;
            nextY = -1;
        }while(!(done = t.esValido(m)));
        main.updateTablero(x, y, t.getTablero()[m.getX()][m.getY()]);
        movements++;

        // realice the real move action
        try {
            partida.realizaAccion(new AccionMover(j, m));
        }
        catch(Exception e) {
            main.showToast("ERROR in GAME: "+j.getNombre()+": "+e.getMessage(), Toast.LENGTH_LONG);
        }

        // realice the move on the server
        if(provider.isOnline()){
            main.setPlayerText(((JugadorHumano) partida.getJugador((turn)%2)).getNombre());

            // do the movement on the server
            server.newMovement(provider.getCurrentUser().getPlayerId(), provider.getCurrentRound().getRoundId(),
                    partida.getTablero().tableroToString(), partida.getTablero().getEstado()==Tablero.FINALIZADA,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
//                            Toast.makeText(ctx, response.toString(), Toast.LENGTH_LONG).show();
                            startTimer();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(ctx, volleyError.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
        }
        else{
            main.setPlayerText(jugador.getNombre());
        }
    }

//    public void realizeRemoteMovement(){
//        String mov = provider.getUltimateMovement();
//        if(!mov.isEmpty()){
//            int[][] tempTab = new int [Constantes.DIMENSION][Constantes.DIMENSION];
//            String [] parts1 = mov.split(",");
//            for(String s : parts1){
//                String [] parts2 = s.split(";");
//                tempTab[Integer.parseInt(parts2[0])][Integer.parseInt(parts2[1])]=Integer.parseInt(parts2[2]);
//            }
//
//            int x=0;
//            int y=0;
//            int player=-1;
//            TableroTresEnRaya tt = (TableroTresEnRaya)partida.getTablero();
//            for (int row = 0; row < Constantes.DIMENSION; row++) {
//                for (int column = 0; column < Constantes.DIMENSION; column++) {
//                    if(tt.getTablero()[column][row] != tempTab[column][row]){
//                        x = column; y = row;
//                        player = tempTab[column][row];
//                    }
//                }
//            }
//            if(player == -1)
//                return;
//
//            if(player == partida.getTablero().getTurno()){
//                // realice the real move action
//                try {
//                    partida.realizaAccion(new AccionMover(jugadorRemoto, new MovimientoTresEnRaya(x,y)));
//                }
//                catch(Exception e) {
//                    main.showToast("ERROR in GAME: "+jugadorRemoto.getNombre()+": "+e.getMessage(), Toast.LENGTH_LONG);
//                }
//            }
//        }
//    }

    /**
     * Returns the tablero in a string
     * @return tablero in a string
     */
    public String getTableroString(){
        return partida.getTablero().tableroToString();
    }

    /**
     * Rebuild the whole tablero out of the transmitted string
     * @param tablero string of the tablero
     */
    public void rebuildTablero(String tablero){
        try {
            partida.getTablero().stringToTablero(tablero);
        } catch (ExcepcionJuego excepcionJuego) {
            excepcionJuego.printStackTrace();
        }
        //update the display
        main.updateWholeTablero((TableroTresEnRaya)partida.getTablero());
    }

    public Context getContext() {
        return ctx;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }
}
