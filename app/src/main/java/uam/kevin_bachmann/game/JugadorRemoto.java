//package uam.kevin_bachmann.game;
//
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//
//import org.json.JSONArray;
//
//import es.uam.eps.multij.Evento;
//import es.uam.eps.multij.Jugador;
//import es.uam.eps.multij.Tablero;
//import uam.kevin_bachmann.utils.DataProvider;
//import uam.kevin_bachmann.utils.ServerInterface;
//
///**
// * @author Kevin Bachmann
// */
//public class JugadorRemoto extends JugadorHumano {
//
//    public JugadorRemoto(String nombre, Game game) {
//        super(nombre, game, "");
//    }
//
//    /**
//     * Reacts on a specific event like CAMBIO, CONFIRMA and TURNO.
//     *
//     * CAMBIO: update the screen and check if the games is done or draw
//     * CONFIRMA: nada
//     * TURNO: this event is called when the player has the chance to play.
//     * read in the coordinates to set the cross and check if this move is valid.
//     */
//    @Override
//    public void onCambioEnPartida(Evento evento) {
//
//        //TODO emulieren des remote players
//        switch (evento.getTipo()) {
//            case Evento.EVENTO_CAMBIO:
//                game.eventoCambio(evento);
//                break;
//            case Evento.EVENTO_TURNO:
//                game.eventoTurno(evento, "", true);
//                break;
//        }
//    }
//}
