package uam.kevin_bachmann.game;

import java.util.ArrayList;

import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;
import uam.kevin_bachmann.utils.DataProvider;

/**
 * This class represens a play-board. In this case the play-board is a matrix
 * of 3x3 where the users can insert their marks. The marks of the different users
 * are represented as 1 and 2. First Player has 1 and the second player has 2.
 * @author Kevin Bachmann
 *
 */
public class TableroTresEnRaya extends Tablero {
	
	private int[][] tablero;
	private ArrayList<Movimiento> movimientosValidos;
    private int element;
	
	/**
	 * Constructor initializes the whole variables and sets the tablero to 0
	 */
	public TableroTresEnRaya() {
		estado = EN_CURSO;
		numJugadores = 2;
		turno = 0;
		numJugadas = 0;
		ultimoMovimiento = null;
        element = 0;
		
		movimientosValidos = new ArrayList<Movimiento>();
		tablero = new int[Constantes.DIMENSION][Constantes.DIMENSION];
		
		//fill tablero with zeros to initialize
		for(int x = 0; x < Constantes.DIMENSION; x++){
			for(int y = 0; y < Constantes.DIMENSION; y++){
				tablero[x][y]=0;
			}
		}
		createMovements();
	}
	
	/**
	 * Creates the possible movements for the whole game.
	 * Used to check if the movement is already used and that the computer-player
	 * is able to play with the generated movements.
	 */
	private void createMovements(){
		for(int x = 0; x < Constantes.DIMENSION; x++){
			for(int y = 0; y < Constantes.DIMENSION; y++){
				movimientosValidos.add(new MovimientoTresEnRaya(x,y));
			}
		}
	}
	
	/**
	 * Gets a Movement as parameter which is checked against validity and then
	 * executed for the particular user.
	 */
	@Override
	public void mueve(Movimiento m) throws ExcepcionJuego {
		if(!esValido(m)){
			throw new ExcepcionJuego("Movement Error");
		}else{
			MovimientoTresEnRaya mov = (MovimientoTresEnRaya)m;
			movimientosValidos.remove(mov);
            if(DataProvider.getInstance().isOnline())
			    tablero[mov.getX()][mov.getY()] = element;
            else
                tablero[mov.getX()][mov.getY()] = turno+1;
		}
		ultimoMovimiento = m;
		numJugadas++;
		
		// end of the game
		if(!esFinalizada()){
			turno = ++turno % numJugadores;
		}
	}

	/**
	 * Check if the Game is finished, draw or still going on and set the right status
	 * if needed.
	 */
	public boolean esFinalizada(){
		int winner = 0;
		// ROWS -----------------------------
		if(tablero[0][0] != 0 && tablero[0][0]==tablero[1][0] && tablero[1][0] == tablero[2][0]){
			//row 1
			winner=tablero[0][0];
		}
		if(tablero[0][1] != 0 && tablero[0][1]==tablero[1][1] && tablero[1][1] == tablero[2][1]){
			//row 2
			winner=tablero[0][1];
		}
		if(tablero[0][2] != 0 && tablero[0][2]==tablero[1][2] && tablero[1][2] == tablero[2][2]){
			//row 3
			winner=tablero[0][2];
		}
		// COLUMNS -----------------------------
		if(tablero[0][0] != 0 && tablero[0][0]==tablero[0][1] && tablero[0][1] == tablero[0][2]){
			//column 1
			winner=tablero[0][0];
		}
		if(tablero[1][0] != 0 && tablero[1][0]==tablero[1][1] && tablero[1][1] == tablero[1][2]){
			//column 2
			winner=tablero[1][0];
		}
		if(tablero[2][0] != 0 && tablero[2][0]==tablero[2][1] && tablero[2][1] == tablero[2][2]){
			//column 3
			winner=tablero[2][0];
		}
		// DIAGONALS ----------------------------- 
		if(tablero[0][0] != 0 && tablero[0][0]==tablero[1][1] && tablero[1][1] == tablero[2][2]){
			//diagonal 1
			winner=tablero[1][1];
		}
		if(tablero[2][0] != 0 && tablero[2][0]==tablero[1][1] && tablero[1][1] == tablero[0][2]){
			//diagonal 2
			winner=tablero[1][1];
		}
	
		if(winner != 0){
			// winner found and now the game can be closed/finished
			estado = FINALIZADA;
			return true;
		}
		//if no one has won check if the game is over (tablas==draw)
		if(numJugadas == Constantes.DIMENSION*Constantes.DIMENSION){
			estado = TABLAS;
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if the Movement is valid and can be executed.
	 */
	@Override
	public boolean esValido(Movimiento m) {
		MovimientoTresEnRaya mov = (MovimientoTresEnRaya) m;
		if(movimientosValidos.contains(mov)){
			return true;
		}
		return false;
	}

	/**
	 * Return the valid Movements.
	 */
	@Override
	public ArrayList<Movimiento> movimientosValidos() {
		return movimientosValidos;
	}

	/**
	 * Creates a String out of the tablero which can afterwards be read and 
	 * the tablero can be recreated.
	 */
	@Override
	public String tableroToString() {
		String out="";
		
		for (int row = 0; row < Constantes.DIMENSION; row++) {
		    for (int column = 0; column < Constantes.DIMENSION; column++) {
		        out += column+";"+row+";"+tablero[column][row]+",";
		    }
		}
		return out;
	}

	/**
	 * Gets a String as parameter which can be rebuilt to a complete
	 * tablero. (e.g. for loading a started game)
	 */
	@Override
	public void stringToTablero(String cadena) throws ExcepcionJuego {
		// x;y;val,x;y;val,x;y;val,
		// x;y;val,x;y;val,x;y;val,
		// x;y;val,x;y;val,x;y;val
		tablero = new int [Constantes.DIMENSION][Constantes.DIMENSION];
		String [] parts1 = cadena.split(",");
		for(String s : parts1){
			String [] parts2 = s.split(";");
			tablero[Integer.parseInt(parts2[0])][Integer.parseInt(parts2[1])]=Integer.parseInt(parts2[2]);
		}

	}

	/**
	 * Visual output of the tablero such that it can be used to play on console.
	 */
	@Override
	public String toString() {
		String out="";
		for (int row = 0; row < Constantes.DIMENSION; row++) {
		    for (int column = 0; column < Constantes.DIMENSION; column++) {
		        out += tablero[column][row] + "\t";
		    }
		    out += "\n";
		}
		return out;
	}

    public int[][] getTablero(){
        return tablero;
    }

    public void setElement(int element){
        this.element = element;
    }
}
