package uam.kevin_bachmann.utils;

import android.app.IntentService;
import android.content.Intent;

import es.uam.eps.multij.Jugador;
import uam.kevin_bachmann.game.Game;

/**
 * @author Kevin Bachmann
 */
public class IsMyTurnService extends IntentService {
    public IsMyTurnService() {
        super("IsMyTurnService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        DataProvider provider = DataProvider.getInstance();
        Jugador j = provider.getCurrentPlayer();

        if(j.puedeJugar(null)){
            sendBroadcast(new Intent(Game.ISMYTURN_SERVICE));
        }
    }
}
