package uam.kevin_bachmann.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import uam.kevin_bachmann.activities.SettingsActivity;
import uam.kevin_bachmann.game.JugadorHumano;
import uam.kevin_bachmann.models.Round;
import uam.kevin_bachmann.models.User;

/**
 * Static class which serves as data provider to use the current logged in user
 * the context, the database in all possible situations with a singleton pattern.
 * @author Kevin Bachmann
 */
public class DataProvider {

    private Context context;
    private DatabaseAdapter db;

    private User currentUser;
    private Round currentRound;
    private JugadorHumano currentPlayer;

    private boolean online;
    private String ultimateMovement;

    private static DataProvider instance;

    private DataProvider(){
        currentUser = null;
        online = false;
        ultimateMovement = "";
    }

    public static DataProvider getInstance(){
        if(instance == null){
            instance = new DataProvider();
        }
        return instance;
    }

    /**
     * Return current logged in user
     * @return current user
     */
    public User getCurrentUser() {
        return currentUser;
    }

    /**
     * Set context and init the database with it
     * @param ctx context
     */
    public void setContext(Context ctx){
        this.context = ctx;
        this.db = new DatabaseAdapter(context);
    }

    /**
     * Set current user and update the preferences.
     * @param currentUser logged in user
     */
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;

        //set preference
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(SettingsActivity.NAME_KEY, currentUser.getName());
        editor.commit();
    }

    /**
     * Set the username of the current user. Preference name.
     * @param name preference name
     */
    public void setName(String name){
        currentUser.setName(name);
        if(!isConnected())
            persistCurrentUser();
    }

    /**
     * UpdateStatistics with won lost movements
     * @param lost
     * @param won
     * @param movements
     */
    public void updateStatistics(int lost, int won, int movements){
        db.open();
        db.updateStatistics(currentUser.getId(), won, lost, movements);
        db.close();
    }

    public void updateStatistics(String email, int lost, int won, int movements){
        db.open();
        User u = db.getUser(email);
        db.updateStatistics(u.getId(), won, lost, movements);
        db.close();
    }

    /**
     * Insert a specific partida with the current state of a game
     * @param tablero current state of the tablero
     */
    public void insertPartida(String tablero){
        db.open();
        db.insertPartida(currentUser.getEmail(), tablero);
        db.close();
    }

    /**
     * Set the last play game of the user
     * @param partida game id
     */
    public void setLastPartida(int partida){
        currentUser.setLastPartida(partida);
        persistCurrentUser();
    }

    /**
     * Get game board
     * @return last game board of the user (last game)
     */
    public String getTablero(){
        return getTablero(currentUser.getLastPartida());
    }

    /**
     * Get game board by passed id
     * @param id user id
     * @return string form of the tablero
     */
    public String getTablero(int id){
        String tablero;

        db.open();
        tablero = db.getTablero(currentUser.getEmail(), id);
        db.close();
        return tablero;
    }

    /**
     * Persist the current informations of the user model
     */
    private void persistCurrentUser(){
        db.open();
        db.updateUser(currentUser);
        db.close();
    }

    /**
     * Retrieve all the statistics data of the current logged in user.
     * @return statistic data with won, lost and movements in this order.
     */
    public int[] getStatistics(){
        db.open();
        int [] stat = db.getStatistics(currentUser.getId());
        db.close();
        return stat;
    }

    public boolean isWifiConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isMobileConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isConnected(){
        return isMobileConnected() || isWifiConnected();
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public Round getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(Round currentRound) {
        this.currentRound = currentRound;
    }

    public JugadorHumano getCurrentPlayer(){
        return currentPlayer;
    }

    public void setCurrentPlayer(JugadorHumano h){
        this.currentPlayer = h;
    }

    public String getUltimateMovement() {
        return ultimateMovement;
    }

    public void setUltimateMovement(String ultimateMovement) {
        this.ultimateMovement = ultimateMovement;
    }
}
