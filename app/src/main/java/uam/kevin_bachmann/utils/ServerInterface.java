package uam.kevin_bachmann.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Kevin Bachmann
 */
public class ServerInterface {

    private static final String BASE_URL = "http://ptha.ii.uam.es/juegosreunidos/";
    private static final int GAME_ID = 132; //109

    private static final String PLAYER_NAME_KEY = "playername";
    private static final String PLAYER_PW_KEY = "playerpassword";

    private static final String GAME_ID_KEY = "gameid";
    private static final String PLAYER_ID_KEY = "playerid";
    private static final String ROUND_ID_KEY = "roundid";
    private static final String TABLERO_KEY = "codedboard";
    private static final String NEXT_KEY = "next";
    private static final String FINISHED_KEY = "finished";
    private static final String MARK_AS_READ_KEY = "markasread";
    private static final String FROM_TIMESTAMP_KEY = "fromdate";
    private static final String TO_USER_KEY = "to";
    private static final String TO_ROUND_KEY = "toround";
    private static final String MSG_KEY = "msg";

    private static final String ACCOUNT_PHP = "account.php";
    public static final String OPEN_ROUNDS_PHP = "openrounds.php";
    public static final String ACTIVE_ROUNDS_PHP = "activerounds.php";
    public static final String FINISHED_ROUNDS_PHP = "finishedrounds.php";
    private static final String NEW_ROUND_PHP = "newround.php";
    private static final String ADD_PLAYER_PHP = "addplayertoround.php";
    private static final String REMOVE_PLAYER_PHP = "removeplayerfromround.php";
    private static final String IS_MY_TURN_PHP = "ismyturn.php";
    private static final String NEW_MOVEMENT_PHP = "newmovement.php";
    private static final String SEND_MSG_PHP = "sendmsg.php";
    private static final String GET_MSGS_PHP = "getmsgs.php";
    private static final String ROUNDHISTORY_PHP = "roundhistory.php";

    private static final String DEBUG_TAG = "InterfazConServidor";

    private static ServerInterface serverInterface;
    private RequestQueue queue;

    private ServerInterface(Context context){
        this.queue = Volley.newRequestQueue(context);
    }

    public static ServerInterface getServer(Context context){
        if(serverInterface == null){
            serverInterface = new ServerInterface(context);
        }
        return serverInterface;
    }

    public void register(final String playername, final String playerpassword,
                Response.Listener<String> callback, Response.ErrorListener errorCallback) {
        String url = BASE_URL + ACCOUNT_PHP;
        Log.d(DEBUG_TAG, url);

        StringRequest request = new StringRequest(Request.Method.POST, url, callback, errorCallback) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(PLAYER_NAME_KEY, playername);
                params.put(PLAYER_PW_KEY, playerpassword);
                return params;
            }
        };
        queue.add(request);
    }

    public void login(final String playername, final String playerpassword,
                      Response.Listener<String> callback, Response.ErrorListener errorCallback) {
        String url = BASE_URL + ACCOUNT_PHP;
        Log.d(DEBUG_TAG, url);

        StringRequest request = new StringRequest(Request.Method.POST, url, callback, errorCallback) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(PLAYER_NAME_KEY, playername);
                params.put(PLAYER_PW_KEY, playerpassword);
                params.put("login", "");
                return params;
            }
        };
        queue.add(request);
    }

    public void newRound(final String playerId, final String tablero,
                        Response.Listener<String> callback, Response.ErrorListener errorCallback){
        String url = BASE_URL + NEW_ROUND_PHP;
        Log.d(DEBUG_TAG, url);

        StringRequest request = new StringRequest(Request.Method.POST, url, callback, errorCallback) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put(GAME_ID_KEY, String.valueOf(GAME_ID));
                params.put(PLAYER_ID_KEY, String.valueOf(playerId));
                if(!tablero.isEmpty()){
                    params.put(TABLERO_KEY, tablero);
                }
                return params;
            }
        };
        queue.add(request);
    }

    public void isMyTurn(final String playerId, final int roundId,
                            Response.Listener<JSONObject> callback, Response.ErrorListener errorCallback){
        String url = BASE_URL + IS_MY_TURN_PHP+"?"
                +PLAYER_ID_KEY+"="+playerId+"&"
                +ROUND_ID_KEY+"="+roundId+
                "&json";
        Log.d(DEBUG_TAG, url);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, callback, errorCallback);
        queue.add(jsonObjectRequest);
    }

    public void newMovement(final String playerId, final int roundId, final String tablero, final boolean finished,
                            Response.Listener<JSONObject> callback, Response.ErrorListener errorCallback){
        String url = BASE_URL + NEW_MOVEMENT_PHP+"?"
                +PLAYER_ID_KEY+"="+playerId+"&"
                +ROUND_ID_KEY+"="+roundId+"&"+
                TABLERO_KEY+"="+tablero+"&";
        if(finished){
            url+= FINISHED_KEY+"="+finished+"&";
        }
        url += "json";
        Log.d(DEBUG_TAG, url);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, callback, errorCallback);
        queue.add(jsonObjectRequest);
    }

    public void addPlayerToRound(final String playerId, final int roundId,
                                 Response.Listener<String> callback, Response.ErrorListener errorCallback){
        String url = BASE_URL + ADD_PLAYER_PHP;
        Log.d(DEBUG_TAG, url);

        StringRequest request = new StringRequest(Request.Method.POST, url, callback, errorCallback) {

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put(PLAYER_ID_KEY, String.valueOf(playerId));
                params.put(ROUND_ID_KEY, String.valueOf(roundId));
                return params;
            }
        };
        queue.add(request);
    }

    public void removePlayerFromRound(final String playerId, final int roundId,
                                      Response.Listener<String> callback, Response.ErrorListener errorCallback){
        // not tested because not used
        String url = BASE_URL + REMOVE_PLAYER_PHP;
        Log.d(DEBUG_TAG, url);

        StringRequest request = new StringRequest(Request.Method.POST, url, callback, errorCallback) {

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put(PLAYER_ID_KEY, String.valueOf(playerId));
                params.put(ROUND_ID_KEY, String.valueOf(roundId));
                return params;
            }
        };
        queue.add(request);
    }




    public void openRounds(final String playerId,
                           Response.Listener<JSONArray> callback, Response.ErrorListener errorCallback){
        String url = BASE_URL + OPEN_ROUNDS_PHP+"?"
                + GAME_ID_KEY+"="+GAME_ID+"&"
                +PLAYER_ID_KEY+"="+playerId
                +"&json";
        Log.d(DEBUG_TAG, url);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, callback, errorCallback);
        queue.add(jsonArrayRequest);
    }

    public void activeRounds(final String playerId,
                             Response.Listener<JSONArray> callback, Response.ErrorListener errorCallback){
        String url = BASE_URL + ACTIVE_ROUNDS_PHP+"?"
                + GAME_ID_KEY+"="+GAME_ID+"&"
                +PLAYER_ID_KEY+"="+playerId
                +"&json";
        Log.d(DEBUG_TAG, url);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, callback, errorCallback);
        queue.add(jsonArrayRequest);
    }

    public void finishedRounds(final String playerId,
                               Response.Listener<JSONArray> callback, Response.ErrorListener errorCallback){
        String url = BASE_URL + FINISHED_ROUNDS_PHP+"?"
                + GAME_ID_KEY+"="+GAME_ID+"&"
                +PLAYER_ID_KEY+"="+playerId
                +"&json";
        Log.d(DEBUG_TAG, url);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, callback, errorCallback);
        queue.add(jsonArrayRequest);
    }

    public void sendMsg(final String playerId, final String to, final int toRound, final String msg,
                        Response.Listener<String> callback, Response.ErrorListener errorCallback){
        // not tested because not used
        String url = BASE_URL + SEND_MSG_PHP;
        Log.d(DEBUG_TAG, url);

        StringRequest request = new StringRequest(Request.Method.POST, url, callback, errorCallback) {

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put(PLAYER_ID_KEY, String.valueOf(playerId));
                params.put(TO_USER_KEY, to);
                params.put(TO_ROUND_KEY, String.valueOf(toRound));
                params.put(MSG_KEY, msg);
                return params;
            }
        };
        queue.add(request);
    }

    public void getMsgs(final String playerId, final boolean markasread,
                        Response.Listener<JSONArray> callback, Response.ErrorListener errorCallback){
        // not tested because not used
        String url = BASE_URL + GET_MSGS_PHP+"?"
                +PLAYER_ID_KEY+"="+playerId+"&"
                +MARK_AS_READ_KEY+"="+ String.valueOf(markasread)
                +"&json";
        Log.d(DEBUG_TAG, url);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, callback, errorCallback);
        queue.add(jsonArrayRequest);
    }

    public void getMsgs(final int roundId, final Timestamp fromDate,
                        Response.Listener<JSONArray> callback, Response.ErrorListener errorCallback){
        // not tested because not used
        String url = BASE_URL + GET_MSGS_PHP+"?"
                +ROUND_ID_KEY+"="+roundId+"&"
                +FROM_TIMESTAMP_KEY+"="+ String.valueOf(fromDate)
                +"&json";
        Log.d(DEBUG_TAG, url);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, callback, errorCallback);
        queue.add(jsonArrayRequest);
    }

    public void roundHistory(final String playerId, final int roundId, final Timestamp fromDate,
                             Response.Listener<JSONArray> callback, Response.ErrorListener errorCallback){
        // not tested because not used
        String url = BASE_URL + ROUNDHISTORY_PHP+"?"
                +PLAYER_ID_KEY+"="+playerId+"&"
                +ROUND_ID_KEY+"="+roundId+"&"
                +FROM_TIMESTAMP_KEY+"="+ String.valueOf(fromDate)
                +"&json";
        Log.d(DEBUG_TAG, url);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, callback, errorCallback);
        queue.add(jsonArrayRequest);
    }
}
