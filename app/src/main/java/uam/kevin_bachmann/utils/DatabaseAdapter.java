package uam.kevin_bachmann.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import uam.kevin_bachmann.models.User;

/**
 * Class to control the database. Within this class all the create, insert, delete and update
 * commands are taken.
 * There exist 3 tables users, partidas and statistics. partidas and statistics are both refered
 * to the user table.
 * @author Kevin Bachmann
 */
public class DatabaseAdapter {
    public static final String ID = "id";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String NAME = "name";
    public static final String LAST_PARTIDA = "lastpartida";

    public static final String PARTIDA = "partida";
    public static final String TABLERO = "tablero";

    public static final String LOST = "lost";
    public static final String WON = "won";
    public static final String MOVEMENTS = "movements";

    private static final String DATABASE_NAME = "tresenraya.db";
    private static final String TABLE_USERS = "users";
    private static final String TABLE_PARTIDAS = "partidas";
    private static final String TABLE_STATISTICS = "statistics";
    private static final int DATABASE_VERSION = 1;

    private DatabaseHelper helper;
    private SQLiteDatabase db;

    public DatabaseAdapter(Context context) {
        helper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        /**
         * Create database.
         * @param db created database
         */
        public void onCreate(SQLiteDatabase db) {
            createTable(db);
        }

        /**
         * Upgrades the database with the new version passed.
         * @param db database
         * @param oldVersion the old version number of the db
         * @param newVersion the new version number of the db
         */
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARTIDAS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATISTICS);
            createTable(db);
        }

        /**
         * Create the specific tables which are defined in this method.
         * 3 tables users, partidas and statistics.
         * @param db
         */
        private void createTable(SQLiteDatabase db) {
            String str1 = "CREATE TABLE " + TABLE_USERS + " ("
                    + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + EMAIL + " TEXT UNIQUE, "
                    + PASSWORD + " TEXT, " + LAST_PARTIDA + " INTEGER, "
                    + NAME + " TEXT);";
            String str2 = "CREATE TABLE " + TABLE_PARTIDAS + " (" + ID
                    + " INTEGER REFERENCES " + TABLE_USERS + ", " + PARTIDA
                    + " INTEGER, "+ TABLERO +" TEXT, PRIMARY KEY (" + ID + ", " + PARTIDA + "));";
            String str3 = "CREATE TABLE " + TABLE_STATISTICS + " (" + ID
                    + " INTEGER REFERENCES " + TABLE_USERS + ", " + WON
                    + " INTEGER, "+ LOST +" INTEGER, "+ MOVEMENTS+ " INTEGER, PRIMARY KEY (" + ID + "));";
            try {
                db.execSQL(str1);
                db.execSQL(str2);
                db.execSQL(str3);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Open the database to make it possible to use it.
     * Furthermore the foreign keys are enabled with the second command.
     * @return
     * @throws SQLException
     */
    public DatabaseAdapter open() throws SQLException {
        db = helper.getWritableDatabase();
        db.execSQL("PRAGMA foreign_keys='ON'");
        return this;
    }

    /**
     * Close the database and save the state.
     */
    public void close() {
        db.close();
    }

    /**
     * USERS
     */

    /**
     * Insert a new user into the database with all the needed information.
     * @param email unique email
     * @param password password
     * @param name username
     * @param last_partida last played partida which refers to the partidas table
     * @return
     */
    public long insertUser(String email, String password, String name, int last_partida) {
        ContentValues values = new ContentValues();
        values.put(EMAIL, email);
        values.put(PASSWORD, password);
        values.put(NAME, name);
        values.put(LAST_PARTIDA, last_partida);

        long id = db.insert(TABLE_USERS, null, values);

        //init statistics
        initStatistics(id);
        return id;
    }

    /**
     * Delete user
     * @param id userId
     * @return true if it worked, false otherwise
     */
    public boolean deleteUser(long id) {
        db.delete(TABLE_PARTIDAS, ID + "=" + id, null);
        return db.delete(TABLE_USERS, ID + "=" + id, null) > 0;
    }

    /**
     * Returns a cursor to access all the users.
     * @return cursor to all the users
     */
    public Cursor getAllUsers() {
        return db.query(TABLE_USERS, new String[] { ID, EMAIL, PASSWORD, NAME},
                null, null, null, null, null);
    }

    /**
     * Get specific user with the unique email.
     * @param email unique email
     * @return searched user or null if the user doesnt exist.
     */
    public User getUser(String email){
        Cursor  cursor = db.rawQuery("SELECT * " +
                "FROM "+TABLE_USERS+" as u WHERE u."+EMAIL+"='"+email+"'", null);

        User u = null;
        if (cursor.moveToFirst()) {
            do {
                int iId = cursor.getColumnIndex(ID);
                int iEmail = cursor.getColumnIndex(EMAIL);
                int iPass = cursor.getColumnIndex(PASSWORD);
                int iName = cursor.getColumnIndex(NAME);
                int iLast = cursor.getColumnIndex(LAST_PARTIDA);

                // get  the  data into array,or class variable
                u = new User(cursor.getInt(iId), "", cursor.getString(iEmail), cursor.getString(iPass),
                        cursor.getString(iName), cursor.getInt(iLast));
            } while (cursor.moveToNext());
        }
        return  u;
    }

    /**
     * Updates the user with the passed user data
     * @param u new data to update
     */
    public void updateUser(User u){
        ContentValues values = new ContentValues();
        values.put(NAME, u.getName());
        values.put(LAST_PARTIDA, u.getLastPartida());
        db.update(TABLE_USERS, values, ID+"="+u.getId(), null);
    }

    /**
     * Checks if the user credentials exist in the db.
     * @param email unique email
     * @param password password
     * @return true if the user exists and the password is correct, false otherwise
     */
    public boolean isRegistered(String email, String password) {
        Cursor cursor = db.query(TABLE_USERS, new String[] { EMAIL, PASSWORD },
                EMAIL + " = '" + email + "' AND " + PASSWORD + "= '" + password + "'",
                null, null, null, EMAIL + " DESC");
        int count = cursor.getCount();

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        if (count == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the user id with the email address.
     * @param email unique email
     * @return user id long
     */
    public int getUserId(String email){
        Cursor cursorId = db.query(TABLE_USERS, new String[] { ID, EMAIL },
                EMAIL + " = '" + email + "'", null, null, null, EMAIL
                        + " DESC");
        cursorId.moveToFirst();
        int index = cursorId.getColumnIndex(ID);
        return cursorId.getInt(index);
    }

    /**
     * PARTIDAS
     */

    /**
     * Insert a partida with the tablero-string.
     * @param email unique email
     * @param tablero tablero string which represents a tablero state and can be reproduced.
     * @return
     */
    public boolean insertPartida(String email, String tablero){
        int id = getUserId(email);

        try {
            Cursor cursorPartida = db.query(TABLE_PARTIDAS, new String[] {
                    "MAX(" + PARTIDA + ")" }, ID + " = '" + id + "'", null, ID, null, PARTIDA);

            int maxPartida = cursorPartida.moveToFirst() ? cursorPartida.getInt(0) : 0;
            DataProvider.getInstance().setLastPartida(maxPartida + 1);

            ContentValues values = new ContentValues();
            values.put(ID, id);
            values.put(PARTIDA, maxPartida + 1);
            values.put(TABLERO, tablero);

            open();
            db.insert(TABLE_PARTIDAS, null, values);
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Get tablero-state with email of the user and the partida number
     * @param email unique email
     * @param partida id of the partida of this user
     * @return tablero-string to rebuild
     */
    public String getTablero(String email, int partida){
        int userId = getUserId(email);
        Cursor cursor = db.query(TABLE_PARTIDAS,new String[]{TABLERO}, ID+"='"+userId+"' AND "+PARTIDA+"='"+partida+"'", null, null, null, null);

        String tablero = "";
        if (cursor.moveToFirst()) {
            do {
                tablero = cursor.getString(cursor.getColumnIndex(TABLERO));
            } while (cursor.moveToNext());
        }
        return tablero;
    }

    /**
     * STATISTICS
     */
    /**
     * Init the statistics of a user with default values.
     * @param userId unique id
     * @return id of the row
     */
    public long initStatistics(long userId){
        ContentValues values = new ContentValues();
        values.put(ID, userId);
        values.put(LOST, 0);
        values.put(WON, 0);
        values.put(MOVEMENTS, 0);
        return db.insert(TABLE_STATISTICS, null, values);
    }

    /**
     * Update the statistics with the passed values of the user.
     * @param userId unique id
     * @param won count of won games (1 or 0)
     * @param lost count of lost games (1 or 0)
     * @param movements movements of one game
     */
    public void updateStatistics(long userId, int won, int lost, int movements){
        //get the statistics of the user and just add won, lost and movements
        try {
            Cursor  cursor = db.rawQuery("SELECT * " +
                    "FROM "+TABLE_STATISTICS+" as s WHERE s."+ID+"='"+userId+"'", null);
            if (cursor.moveToFirst()) {
                do {
                    int w = cursor.getInt(cursor.getColumnIndex(WON));
                    int l = cursor.getInt(cursor.getColumnIndex(LOST));
                    int m = cursor.getInt(cursor.getColumnIndex(MOVEMENTS));

                    won += w;
                    lost += l;
                    movements += m;
                } while (cursor.moveToNext());
            }
            ContentValues values = new ContentValues();
            values.put(LOST, lost);
            values.put(WON, won);
            values.put(MOVEMENTS, movements);
            db.update(TABLE_STATISTICS, values, ID+"="+userId, null);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves all statistics of the database of one user
     * @param userId unique user id
     * @return a int array with won, lost and movements in this order
     */
    public int[] getStatistics(int userId){
        int won = 0;
        int lost = 0;
        int movements = 0;
        Cursor  cursor = db.rawQuery("SELECT * " +
                "FROM "+TABLE_STATISTICS+" as s WHERE s."+ID+"='"+userId+"'", null);
        if (cursor.moveToFirst()) {
            do {
                int w = cursor.getInt(cursor.getColumnIndex(WON));
                int l = cursor.getInt(cursor.getColumnIndex(LOST));
                int m = cursor.getInt(cursor.getColumnIndex(MOVEMENTS));

                won += w;
                lost += l;
                movements += m;
            } while (cursor.moveToNext());
        }
        return new int[]{won, lost, movements};
    }
}
