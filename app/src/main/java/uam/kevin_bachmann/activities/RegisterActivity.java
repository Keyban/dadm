package uam.kevin_bachmann.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import uam.kevin_bachmann.R;
import uam.kevin_bachmann.utils.DataProvider;
import uam.kevin_bachmann.utils.DatabaseAdapter;
import uam.kevin_bachmann.utils.ServerInterface;

/**
 * RegistrationActivity to register a new user with email and password. All the
 * textfields has to be filled and the user with the email mustnt exist.
 * @author Kevin Bachmann
 */
public class RegisterActivity extends ActionBarActivity implements View.OnClickListener {

    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextConfirmedPassword;
    private DatabaseAdapter db;
    private ServerInterface server;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editTextEmail = (EditText)findViewById(R.id.emailRegister);
        editTextPassword = (EditText)findViewById(R.id.passwordRegister);
        editTextConfirmedPassword =
                (EditText)findViewById(R.id.passwordRegisterConfirm);

        Button acceptButton = (Button)findViewById(R.id.btn_register2);
        acceptButton.setOnClickListener(this);

        server = ServerInterface.getServer(RegisterActivity.this);
        db = new DatabaseAdapter(this);
    }

    /**
     * Registers a user with email and password if the email doesnt already exist.
     */
    private void register(){
        final String email = editTextEmail.getText().toString();
        final String pass = editTextPassword.getText().toString();
        String confPass = editTextConfirmedPassword.getText().toString();

        if (!pass.equals("") && !email.equals("") && pass.equals(confPass)) {

            final DataProvider dataProvider = DataProvider.getInstance();

            final boolean[] done = {false};
            if(dataProvider.isConnected()) {
                server.register(email, pass, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        if (!s.equals("-1")) {

                            // now in the local database as well
                            db.open();
                            long id = db.insertUser(email, pass, email, -1);
                            db.close();

                            if (id == -1) {
                                editTextEmail.setText("");
                                Toast.makeText(RegisterActivity.this, R.string.toast_user_exists,
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(RegisterActivity.this, R.string.toast_registered,
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            editTextEmail.setText("");
                            editTextPassword.setText("");
                            editTextConfirmedPassword.setText("");
                            Toast.makeText(RegisterActivity.this, R.string.toast_user_exists,
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(RegisterActivity.this,
                                volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                        return;
                    }
                });
            }
        }
        else if(pass.equals("") || confPass.equals("") || email.equals("")) {
            Toast.makeText(RegisterActivity.this,
                    R.string.toast_inserts_empty, Toast.LENGTH_SHORT).show();
        }
        else if(!pass.equals(confPass)) {
            Toast.makeText(RegisterActivity.this, R.string.toast_register_failed, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register2:
                register();
                break;
        }
    }
}
