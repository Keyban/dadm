package uam.kevin_bachmann.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import uam.kevin_bachmann.R;
import uam.kevin_bachmann.utils.DataProvider;

/**
 * This activity controls the statistics view which allows a user to show how many
 * games he lost and won and how many moves he made.
 * @author Kevin Bachmann
 */
public class StatisticsActivity extends ActionBarActivity {

    private TextView won;
    private TextView lost;
    private TextView movements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        int []stat = DataProvider.getInstance().getStatistics();

        won = (TextView)findViewById(R.id.text_won);
        won.setText(String.valueOf(stat[0]));

        lost = (TextView)findViewById(R.id.text_lost);
        lost.setText(String.valueOf(stat[1]));

        movements = (TextView)findViewById(R.id.text_movements);
        movements.setText(String.valueOf(stat[2]));

//        setContentView(R.layout.activity_master_detail);

//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        SettingsFragment fragment = new SettingsFragment();
//        fragmentTransaction.replace(android.R.id.content, fragment);
//        fragmentTransaction.commit();
    }
}
