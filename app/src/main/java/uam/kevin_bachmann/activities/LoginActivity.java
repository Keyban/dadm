package uam.kevin_bachmann.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import uam.kevin_bachmann.R;
import uam.kevin_bachmann.models.User;
import uam.kevin_bachmann.utils.DataProvider;
import uam.kevin_bachmann.utils.DatabaseAdapter;
import uam.kevin_bachmann.utils.ServerInterface;

/**
 * LoginActivity to login with email and password if the user exists.
 * @author Kevin Bachmann
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    private EditText editTextEmail;
    private EditText editTextPassword;

    private DatabaseAdapter db;
    private ServerInterface server;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextEmail = (EditText)findViewById(R.id.emailLogin);
        editTextPassword = (EditText)findViewById(R.id.passwordLogin);

        Button loginButton = (Button)findViewById(R.id.btn_login);
        loginButton.setOnClickListener(this);

        Button registerButton = (Button)findViewById(R.id.btn_register);
        registerButton.setOnClickListener(this);

        server = ServerInterface.getServer(this.getApplicationContext());
        DataProvider.getInstance().setContext(LoginActivity.this);
    }

    /**
     * Login with the inserted email and password.
     * All the textfields has to be filled and correct. Sends a request to the db.
     */
    private void login(){

        final String email = editTextEmail.getText().toString();
        final String pass = editTextPassword.getText().toString();

        if (!pass.equals("") && !email.equals("")) {
            final DataProvider dataProvider = DataProvider.getInstance();
            dataProvider.setContext(this);
            db = new DatabaseAdapter(this);

            if(dataProvider.isConnected()){
                server.login(email, pass, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        if(!s.equals("-1")){

                            // check if the user exists in the local db as well. if not save it
                            db.open();
                            boolean registered = db.isRegistered(email, pass);
                            if(!registered){
                                db.insertUser(email, pass, email, -1);
                            }
                            User u = db.getUser(email);
                            u.setPlayerId(s);
                            db.close();
                            dataProvider.setCurrentUser(u);

                            Intent intent = new Intent(LoginActivity.this, ModeActivity.class);
                            startActivity(intent);
                            finish();
                        } else{
                            Toast.makeText(LoginActivity.this, R.string.toast_not_registered,Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(LoginActivity.this,
                                volleyError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            } else{
                db.open();
                boolean registered = db.isRegistered(email, pass);

                User u = null;
                if(registered){
                    u = db.getUser(email);
                }
                db.close();

                if(registered){
                    dataProvider.setCurrentUser(u);
                    Intent intent = new Intent(LoginActivity.this, ModeActivity.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    Toast.makeText(LoginActivity.this, R.string.toast_not_registered,Toast.LENGTH_SHORT).show();
                }
            }
        }
        else if(pass.equals("") ||  email.equals("")) {
            Toast.makeText(LoginActivity.this,
                    R.string.toast_inserts_empty, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_login:
                login();
                break;
        }
    }
}
