package uam.kevin_bachmann.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import uam.kevin_bachmann.R;
import uam.kevin_bachmann.models.Round;
import uam.kevin_bachmann.utils.DataProvider;
import uam.kevin_bachmann.utils.SeparatedListAdapter;
import uam.kevin_bachmann.utils.ServerInterface;

/**
 * Activity in which the current pending games of the server are displayed.
 * @author Kevin Bachmann
 */
public class GamesActivity extends ActionBarActivity implements View.OnClickListener{

    private ArrayList<String> openGames;
    private ArrayList<String> activeGames;
    private ArrayList<String> finishedGames;

    private Map<Integer, Round> rounds;

    private ServerInterface server;
    private Timer timer;
    private static final int TIMER_SEC = 10;

    public static final String REFRESHGAME_SERVICE =
            "uam.kevin_bachmann.REFRESHGAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);

        //check if connection is established
        if(!DataProvider.getInstance().isConnected()){
            Toast.makeText(this, "No internet connection available.", Toast.LENGTH_LONG).show();
            return;
        }

        openGames = new ArrayList<>();
        activeGames = new ArrayList<>();
        finishedGames = new ArrayList<>();

        rounds = new HashMap<>();

        Button addGame = (Button) findViewById(R.id.btn_add_game_online);
        addGame.setOnClickListener(this);

        ListView lv = (ListView) findViewById(R.id.game_list);
        SeparatedListAdapter adapter = new SeparatedListAdapter(GamesActivity.this);
        lv.setAdapter(adapter);

        server = ServerInterface.getServer(GamesActivity.this);
        loadRounds();

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                loadRounds();
            }
        }, TIMER_SEC*1000, TIMER_SEC*1000);
    }

    @Override
    public void onResume(){
        loadRounds();

        if(timer == null)
           timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                loadRounds();
            }
        }, TIMER_SEC*1000, TIMER_SEC*1000);

        super.onResume();
    }

    @Override
    public void onPause(){
        if(timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }

        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch(id){
            case R.id.action_settings:
                // show settings action
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_help:
                // show help action
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_statistics:
                // show statistics action
                intent = new Intent(this, StatisticsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                // show statistics action
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_play_online:
                Toast.makeText(this, "Not yet implemented.", Toast.LENGTH_LONG).show();
                break;
            case R.id.btn_add_game_online:
                server.newRound(DataProvider.getInstance().getCurrentUser().getPlayerId(), "", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        if(s.equals("-1")){
                            Toast.makeText(GamesActivity.this, getApplicationContext().getString(R.string.new_round_error), Toast.LENGTH_LONG).show();
                            return;
                        } else{
                            Toast.makeText(GamesActivity.this, "Round: "+ s, Toast.LENGTH_LONG).show();

                            loadRounds();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(GamesActivity.this, volleyError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
                break;
        }
    }

    private void loadRounds(){
        //OPEN
        server.openRounds(DataProvider.getInstance().getCurrentUser().getPlayerId(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                try {
                    openGames = new ArrayList<>();
                    parseRoundJSON(ServerInterface.OPEN_ROUNDS_PHP, jsonArray);
                } catch (JSONException e) {
                    Toast.makeText(GamesActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(GamesActivity.this, volleyError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        //ACTIVE
        server.activeRounds(DataProvider.getInstance().getCurrentUser().getPlayerId(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                try {
                    activeGames = new ArrayList<>();
                    parseRoundJSON(ServerInterface.ACTIVE_ROUNDS_PHP, jsonArray);
                } catch (JSONException e) {
                    Toast.makeText(GamesActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(GamesActivity.this, volleyError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        //FINISHED
        server.finishedRounds(DataProvider.getInstance().getCurrentUser().getPlayerId(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                try {
                    finishedGames = new ArrayList<>();
                    parseRoundJSON(ServerInterface.FINISHED_ROUNDS_PHP, jsonArray);
                } catch (JSONException e) {
                    Toast.makeText(GamesActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(GamesActivity.this, volleyError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void parseRoundJSON (String type, JSONArray response) throws JSONException {
        ListView lv = (ListView) findViewById(R.id.game_list);
        SeparatedListAdapter adapter = (SeparatedListAdapter)lv.getAdapter();

        for (int i=0; i<response.length(); i++){
            JSONObject score = (JSONObject) response.get(i);

            //roundid, numberofplayers, dateevent, playernames, turn, codeboard
            String roundId = score.getString("roundid");
            String dateevent = score.getString("dateevent");
            String playernames = score.getString("playernames");
            String turn = score.getString("turn");
            String codedboard = score.getString("codedboard");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSZZZ");
            Date date = null;
            try {
                date = sdf.parse(dateevent);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Timestamp timestamp = null;
            if(date != null)
                timestamp = new Timestamp(date.getTime());

            rounds.put(Integer.valueOf(roundId), new Round(codedboard,playernames,Integer.valueOf(roundId), timestamp, Integer.valueOf(turn)));

            sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
            String gameText = "RoundId: " + roundId+"\n" +
                    "Date: "+sdf.format(date)+"\n" +
                    "Players: "+playernames+"\n" +
                    "Turn: "+playernames.split(",")[Integer.valueOf(turn)-1];

            switch(type){
                case ServerInterface.OPEN_ROUNDS_PHP:
                    if(!openGames.contains(gameText)) {
                        openGames.add(gameText);
                    }
                    break;
                case ServerInterface.ACTIVE_ROUNDS_PHP:
                    if(!activeGames.contains(gameText)) {
                        activeGames.add(gameText);
                    }
                    break;
                case ServerInterface.FINISHED_ROUNDS_PHP:
                    if(!finishedGames.contains(gameText)) {
                        finishedGames.add(gameText);
                    }
                    break;
            }
        }
        adapter.addSection(getApplicationContext().getString(R.string.status_active), new ArrayAdapter<>(GamesActivity.this, R.layout.list_item, R.id.game_state, activeGames));
        adapter.addSection(getApplicationContext().getString(R.string.status_open), new ArrayAdapter<>(GamesActivity.this, R.layout.list_item, R.id.game_state, openGames));
        adapter.addSection(getApplicationContext().getString(R.string.status_finished), new ArrayAdapter<>(GamesActivity.this, R.layout.list_item, R.id.game_state, finishedGames));

        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                final String text = (String)parent.getItemAtPosition(position);
                int roundId = Integer.valueOf(text.substring(9).split("\n")[0]);
                DataProvider provider = DataProvider.getInstance();

                Round round = rounds.get(roundId);
                provider.setCurrentRound(round);
                String tablero = round.getTablero();

                String[] players = round.getPlayers().split(",");

                if(openGames.contains(text)){
                    if(!provider.getCurrentUser().getEmail().equals(players[0])) {
                        server.addPlayerToRound(provider.getCurrentUser().getPlayerId(), roundId, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                if (s.equals("0")) {
                                    Toast.makeText(GamesActivity.this, getApplicationContext().getString(R.string.add_player_error), Toast.LENGTH_LONG).show();
                                    return;
                                }

                                Toast.makeText(GamesActivity.this, getApplicationContext().getString(R.string.added_player_success), Toast.LENGTH_LONG).show();
                                loadRounds();
                                openGames.remove(text);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                Toast.makeText(GamesActivity.this, volleyError.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    else{
                        Toast.makeText(GamesActivity.this, getApplicationContext().getString(R.string.same_user), Toast.LENGTH_LONG).show();
                    }
                } else if(activeGames.contains(text)){
                    //TODO start or continue game
                    Intent intent = new Intent(GamesActivity.this, TableroActivity.class);
                    intent.putExtra("round",round);
                    startActivity(intent);

                } else if(finishedGames.contains(text)){
                    // TODO open tablero activity and init done game
                    Intent intent = new Intent(GamesActivity.this, TableroActivity.class);
                    intent.putExtra("round",round);
                    startActivity(intent);
                }
            }
        });
    }
}
