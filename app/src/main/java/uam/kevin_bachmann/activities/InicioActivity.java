package uam.kevin_bachmann.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import uam.kevin_bachmann.R;

/**
 * Initialize Activity which shows an activity while the application gets ready.
 * @author Kevin Bachmann
 */
public class InicioActivity extends Activity {

    private final int WAIT_TIME = 2000;

    private Animation rotate;
    private Animation mixed;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_inicio);

        rotate = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate);
        mixed = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.mixed);

        image = (ImageView) findViewById(R.id.tictactoe);
        image.startAnimation(mixed);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(InicioActivity.this, LoginActivity.class);
                InicioActivity.this.startActivity(intent);
                InicioActivity.this.finish();
            }
        }, WAIT_TIME);
    }
}
