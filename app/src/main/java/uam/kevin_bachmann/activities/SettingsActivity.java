package uam.kevin_bachmann.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import uam.kevin_bachmann.R;
import uam.kevin_bachmann.fragments.SettingsFragment;

/**
 * Activity which contains the settings management and fragment.
 * The settings are the username and the background color.
 * @author Kevin Bachmann
 */
public class SettingsActivity extends ActionBarActivity {

    public static final String NAME_KEY = "playerName";
    public static final String COLOR_KEY = "color";
    public static final String COLOR_DEF = "white";
//    public static final String ONLINE_KEY = "workWithConnection";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_void);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SettingsFragment fragment = new SettingsFragment();
        fragmentTransaction.replace(android.R.id.content, fragment);
        fragmentTransaction.commit();
    }
}
