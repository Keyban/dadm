package uam.kevin_bachmann.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import uam.kevin_bachmann.R;
import uam.kevin_bachmann.game.Constantes;
import uam.kevin_bachmann.game.Game;
import uam.kevin_bachmann.game.TableroTresEnRaya;
import uam.kevin_bachmann.models.Round;
import uam.kevin_bachmann.utils.DataProvider;

/**
 * This is the Tablero Activity of the App and here all the events of the user interface
 * get caught and will be handled.
 *
 * @author Kevin Bachmann
 */
public class TableroActivity extends ActionBarActivity implements View.OnClickListener {

    private final int ids[][] = {{R.id.btn_0_0, R.id.btn_1_0, R.id.btn_2_0},
                                {R.id.btn_0_1, R.id.btn_1_1, R.id.btn_2_1},
                                {R.id.btn_0_2, R.id.btn_1_2, R.id.btn_2_2}};
    private Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_tablero_tablet);
        }else{
            setContentView(R.layout.activity_tablero);
        }

        if(DataProvider.getInstance().isOnline()){
            (findViewById(R.id.btnContinue)).setEnabled(false);
        }

        registerListeners();
        this.game = new Game(this);
        game.start(true);

        if(getIntent().getExtras() != null){
            Round round = (Round)getIntent().getExtras().getSerializable("round");
            String tablero = round.getTablero();
            game.setTurn(round.getTurn());
            if(!tablero.isEmpty()){
                game.rebuildTablero(tablero);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch(id){
            case R.id.action_settings:
                // show settings action
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_help:
                // show help action
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_statistics:
                // show statistics action
                intent = new Intent(this, StatisticsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                // show statistics action
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause(){
        //save the tablero in the db
        if(DataProvider.getInstance().isOnline()){
            if(Game.receiver !=null)
                unregisterReceiver(Game.receiver);
            game.stopTimer();
        }
        else{
            DataProvider.getInstance().insertPartida(game.getTableroString());
        }

        super.onPause();
    }

    @Override
    public void onResume(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String color = sharedPreferences.getString(SettingsActivity.COLOR_KEY, SettingsActivity.COLOR_DEF);

        View view = getWindow().getDecorView().getRootView();
        if(color.equals("blue")){
            view.setBackgroundColor(Color.argb(150, 0, 0, 120));
        }else if(color.equals("white")){
            view.setBackgroundColor(Color.argb(255, 255, 255, 255));
        }else if(color.equals("green")){
            view.setBackgroundColor(Color.argb(150, 0, 120, 0));
        }

        // reload the last partida from the database
        DataProvider prov = DataProvider.getInstance();
        if(prov.isOnline()){
            String tablero = prov.getCurrentRound().getTablero();
            if(!tablero.isEmpty())
                game.rebuildTablero(tablero);

            registerReceiver(Game.receiver, new IntentFilter(Game.ISMYTURN_SERVICE));
            game.startTimer();
        }
        else{
            if(prov.getCurrentUser().getLastPartida()!=-1){
                game.rebuildTablero(prov.getTablero());
            }
        }
        super.onResume();
    }

    /**
     * Register all the on click event listeners of all the buttons in the activity to
     * be able to react on their events.
     */
    private void registerListeners () {
        Button button;
        for (int i=0; i< Constantes.DIMENSION; i++){
            for (int j=0; j<Constantes.DIMENSION; j++) {
                if (ids[i][j] != 0) {
                    button = (Button) findViewById(ids[i][j]);
                    button.setOnClickListener(this);
                }
            }
        }
        button= (Button) findViewById(R.id.btnContinue);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button btn = (Button)v;
                btnContinue_clicked(btn);
            }
        });
    }

    /**
     * On click event of the play again button.
     * @param b play again button object
     */
    private void btnContinue_clicked(Button b){
        clearTablero();
        game.nextTurn(-1, -1);
        game.start(true);
    }

    /**
     * On click event of all buttons. With the parameter it is checked which button is
     * pushed and the event is handled.
     * @param v View that has thrown the event
     */
    @Override
    public void onClick(View v) {
        int x = 0;
        int y = 0;
        Button btn = (Button)v;
        for(int i = 0; i < Constantes.DIMENSION; i++) {
            for (int j = 0; j < Constantes.DIMENSION; j++) {
                if (ids[i][j] == btn.getId()) {
                    x = j;
                    y = i;
                }
            }
        }
        game.nextTurn(x, y);
    }

    /**
     * Update the whole tablero with the played fields from the game-tablero.
     * NOT USED AT THE MOMENT (see upadateTablero -> more effienct)
     * @param tablero tablero-array of the game
     */
    public void updateWholeTablero(final TableroTresEnRaya tablero){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int [][] t = tablero.getTablero();
                Button button;
                for (int row = 0; row < Constantes.DIMENSION; row++) {
                    for (int column = 0; column < Constantes.DIMENSION; column++) {
                        button = (Button) findViewById(ids[column][row]);
                        if(t[row][column] == 1){
                            button.setText(getApplicationContext().getString(R.string.sign1));
                            button.setTextColor(Color.RED);
                        } else if (t[row][column] == 2){
                            button.setText(getApplicationContext().getString(R.string.sign2));
                            button.setTextColor(Color.GREEN);
                        }
                    }
                }
            }
        });
    }

    /**
     * Just update the defined x and y in the tablero and not the whole tablero.
     * @param x the x coordinate to update on the display
     * @param y the y coordinate to update on the display
     * @param turno the player which is currently playing
     */
    public void updateTablero(final int x, final int y, final int turno){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button button = (Button) findViewById(ids[y][x]);
                if(turno== 1){
                    button.setText(getApplicationContext().getString(R.string.sign1));
                    button.setTextColor(Color.RED);
                } else if (turno == 2){
                    button.setText(getApplicationContext().getString(R.string.sign2));
                    button.setTextColor(Color.GREEN);
                }
            }
        });
    }

    /**
     * Clear the tablero of all played spaces.
     */
    public void clearTablero(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button button;
                for (int row = 0; row < Constantes.DIMENSION; row++) {
                    for (int column = 0; column < Constantes.DIMENSION; column++) {
                        button = (Button) findViewById(ids[column][row]);
                        button.setText(getApplicationContext().getString(R.string.signEmpty));
                    }
                }
            }
        });
    }

    /**
     * Shows a toast with the message and duration in the UI-Thread. That means that
     * it can be called from any thread in the system.
     * @param message
     * @param duration
     */
    public void showToast(final String message, final int duration){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TableroActivity.this, message, duration).show();
            }
        });
    }

    /**
     * Sets the text of the player who is currently playing.
     * @param player
     */
    public void setPlayerText(final String player){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView text = (TextView) findViewById(R.id.textPlayer);
                text.setText("Next turn: "+player);
            }
        });
    }
}
