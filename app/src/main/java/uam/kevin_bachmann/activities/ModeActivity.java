package uam.kevin_bachmann.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import uam.kevin_bachmann.R;
import uam.kevin_bachmann.utils.DataProvider;

/**
 * Activity which lets the user decide if he wants to play online or offline.
 * @author Kevin Bachmann
 */
public class ModeActivity extends ActionBarActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode);

        Button onlineButton = (Button)findViewById(R.id.btn_play_online);
        onlineButton.setOnClickListener(this);

        Button offlineButton = (Button)findViewById(R.id.btn_play_offline);
        offlineButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch(v.getId()){
            case R.id.btn_play_online:
                DataProvider.getInstance().setOnline(true);
                intent = new Intent(ModeActivity.this, GamesActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_play_offline:
                DataProvider.getInstance().setOnline(false);
                intent = new Intent(ModeActivity.this, TableroActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch(id){
            case R.id.action_settings:
                // show settings action
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_help:
                // show help action
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_statistics:
                // show statistics action
                intent = new Intent(this, StatisticsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                // show statistics action
                intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
