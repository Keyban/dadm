package uam.kevin_bachmann.activities;

import android.app.Activity;
import android.os.Bundle;

import uam.kevin_bachmann.R;

/**
 * A Activity which displays some information about the application.
 * @author Kevin Bachmann
 */
public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
